/*
a. What directive is used by Node.js in loading the modules it needs?
    Answer: require
b. What Node.js module contains a method for server creation?
    Answer: http module
c. What is the method of the http object responsible for creating a server using Node.js?
    Answer: createServer()
d. What method of the response object allows us to set status codes and content types?
    Answer: writeHead
e. Where will console.log() output its contents when run in Node.js?
    Answer: terminal
f. What property of the request object contains the address' endpoint?
    Answer:  url
*/

const http = require('http');

const port = 3000;

const server = http.createServer((request, response) => {

    if(request.url == '/login'){
        response.writeHead(200, {'content-Type': 'text/plain'});
        response.end("Welcome to the login page");
    } else {
        response.writeHead(404, {'Content-Type': 'text/plain'})
        response.end("I'm sorry the page you are looking for cannot be found");
    }    

});

server.listen(port);

console.log(`Server now accessible at localhost: ${port}`);